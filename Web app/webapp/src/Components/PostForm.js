import React from 'react';

class PostForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			text: "",
		};

		this.handleTextChange = this.handleTextChange.bind(this);
		this.addPost = this.addPost.bind(this);
	}

	handleTextChange(event) {
		this.setState({text: event.target.value});
	}

	addPost() {
		fetch('http://yue.yaeko.fr/posts', {
			method: "POST",
			headers: {
				'Auth_Token': localStorage.getItem('Auth_Token'),
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({postText: this.state.text})
		})
		.then(response => response.json())
		.then(response => {
			alert(response.message);
			this.setState({text: ""});
		});
	}

	render() {
		return(
			<div>
				<form>
				 	<textarea value={this.state.text} onChange={this.handleTextChange} />
				 	
				</form>
				<button onClick={this.addPost}>
					Post
				</button>
			</div>
		);
	}
}

export default PostForm;
