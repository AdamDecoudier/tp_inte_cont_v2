import React from 'react';

import LoginPage from './LoginPage.js';
import News from './News.js';
import PostForm from './PostForm.js';

class Main extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			needAuth: true
		};
	}

	componentWillMount() {
		let token = localStorage.getItem('Auth_Token');
		if (token == null || token == "") {
			this.setState({needAuth: true});
		} else {
			this.setState({needAuth: false});
		}
	}

	logout() {
		localStorage.removeItem('Auth_Token');
		window.location.reload();
	}

	render() {
		if(this.state.needAuth) {
			return(
				<LoginPage />
			);
		}

		return (
			<html>
				<body>
					<div class="mainPannel">
						<button onClick={this.logout}>
							Logout
						</button>
						<PostForm />
						<News />
					</div>
				</body>
			</html>
		);
	}
}

export default Main;