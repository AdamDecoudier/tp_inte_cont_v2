import React from 'react';
import axios from 'axios';

class LoginPage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			login: "",
			password: "",
			loginMessage: "",
		};

		this.handleLoginChange = this.handleLoginChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.login = this.login.bind(this);
	}

	login() {
		localStorage.setItem('Auth_Token', 'a');
		window.location.reload();
	}

	handleLoginChange(event) {
		this.setState({login: event.target.value});
	}

	handlePasswordChange(event) {
		this.setState({password: event.target.value});
	}

	login() {
		console.log(this.state.login);
		console.log(this.state.password);
		fetch('http://yue.yaeko.fr/login', {
			method: "POST",
			headers: {
			'Content-Type': 'application/json'
			},
			body: JSON.stringify({login: this.state.login, password: this.state.password})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response);
			if (response.status != 0) {
				this.setState({loginMessage: response.message});
			} else {
				localStorage.setItem('Auth_Token', response.token);
				window.location.reload();
			}
		})
		.catch(function(error) {
			console.log(error);
		})
	}

	render() {
		return(
			<html>
				<body>
					<div class="loginPage">
						<p>Login page</p>
						<form class="loginForm">
							<div class="loginInput">
								<label>
									Login : 
									<input type="text" value={this.state.login} placeholder="Login" onChange={this.handleLoginChange} />
								</label>
							</div>
							<div class="passwordInput">
								<label>
									Password : 
									<input type="password" value={this.state.password} placeholder="Password" onChange={this.handlePasswordChange} />
								</label>
							</div>
						</form>
						<button class="loginButton" onClick={this.login}>
							Login
						</button>
					</div>
					<div>{this.state.loginMessage}</div>
				</body>
			</html>
		);
	}
	}

export default LoginPage;
