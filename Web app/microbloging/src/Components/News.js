import React from 'react';

class News extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			posts: []
		};

		this.generateNewsItems = this.generateNewsItems.bind(this);
		this.getNews = this.getNews.bind(this);
	}

	logout() {
		localStorage.removeItem('Auth_Token');
		window.location.reload();
	}

	componentWillMount() {
		this.getNews();
	}

	getNews() {
		fetch('http://127.0.0.1:5001/posts', {
			method: "GET",
			headers: {
				'Auth_Token': localStorage.getItem('Auth_Token')
			}
		})
		.then(response => response.json())
		.then(response => {
			this.setState({posts: response.postsList});
		});
	}

	generateNewsItems() {
		let array = this.state.posts
		if (array.length > 0) {
			console.log(array.posts);
			return array.map(function(post){
				return(
					<div key={post.id} class="postElement">
						<div class="postPublisher">From {post.publisher}</div>
						<div class="postDate">On {post.date} at {post.time}</div>
						<div class="postText">{post.text}</div>
					</div>
				)
			})
		} else {
			return(<div>No post found</div>)
		}
	}

	render() {
		let news = this.state.posts;

		return(
			<div className="newsContainer">
				<p class="title">Page des news</p>
				<button onClick={this.getNews}>
					Refresh
				</button>
				{this.generateNewsItems()}
			</div>
		);
	}
}

export default News;