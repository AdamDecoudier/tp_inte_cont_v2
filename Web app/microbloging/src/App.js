import React from 'react';

import style from './css/style.css';

import Main from './Components/Main.js';

function App() {

	return (
		<Main />
	);
}

export default App;
