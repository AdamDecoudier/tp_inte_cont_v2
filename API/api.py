from flask import Flask, request, jsonify
from dataBaseConnector import DatabaseConnector
from flask_cors import CORS

from datetime import datetime
import jwt
import logging

app = Flask(__name__)
CORS(app)

SECRET_KEY = "someRandomSecretKey"
TOKEN_VALIDITY = 3600

def generateToken(login):
	now = datetime.now().timestamp()
	max_validity = now + TOKEN_VALIDITY
	encoded = jwt.encode(
		{
			'login': login,
			'max_validity': max_validity
		}, SECRET_KEY, algorithm='HS256').decode('utf-8')
	return encoded

def verifyToken(token):
	decoded = jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
	if decoded["max_validity"] < datetime.now().timestamp():
		return {"status": 1, "message": "Token has expired"}
	return {"status": 0, "message": "Token successfully decoded", "datas": decoded}


@app.route('/')
def hello_world():
	return 'Hello, World!'



# Login management

@app.route('/login', methods=['POST'])
def login():
	print(request.json)
	if "login" not in request.json or "password" not in request.json:
		return "Missing credentials", 400
	login		= request.json['login']
	password	= request.json['password']
	print("---", login, "---", password, "---")
	dbConnector = DatabaseConnector()
	userExists = dbConnector.verifyLoginInformations(login, password)
	if userExists == 1:
		token = generateToken(login)
		return jsonify({"status": 0, "message": "Successful login", "token": token}), 200
	else:
		return jsonify({"status": 1, "message": "Invalid credentials"}), 401
	return jsonify({"status": 0, "message": "Successful login", "token": generateToken(login)}), 200


# Posts management


# Get all posts
@app.route('/posts', methods=['GET'])
def getPosts():
	if "Auth_Token" not in request.headers:
		return "Missing Token", 400
	elif request.headers["Auth_Token"] == "":
		return "Token can not be null", 400

	token = request.headers['Auth_Token']
	verif = verifyToken(token)

	if verif["status"] != 0:
		return verif["message"], 400

	dbConnector = DatabaseConnector()
	postsList = dbConnector.getPosts()

	return jsonify({"postsList": postsList}), 200


# Add new post
@app.route('/posts', methods=['POST'])
def addPost():
	if "Auth_Token" not in request.headers:
		return jsonify({"message": "Missing Token"}), 400
	elif request.headers["Auth_Token"] == "":
		return jsonify({"message": "Token can not be null"}), 400

	token = request.headers['Auth_Token']
	verif = verifyToken(token)

	if verif["status"] != 0:
		return jsonify({"message": verif["message"]}), 400

	if "postText" not in request.json:
		return jsonify({"message": "You must specify text"}), 400
	elif request.json["postText"] == "":
		return jsonify({"message": "Text can not be null"}), 400

	text = request.json["postText"]

	dbConnector = DatabaseConnector()
	addPost = dbConnector.addPost(text, verif["datas"]["login"])
	if addPost["status"] != 0:
		return jsonify({"message": addPost["message"]}), 400

	print(addPost["message"])
	return jsonify({"message": addPost["message"]}), 200

app.run(host='0.0.0.0', port='5000')
