import mysql.connector
import logging
from mysql.connector import errorcode

from datetime import datetime

class DatabaseConnector:

	connexion = None
	cursor = None

	def __init__(self):
		print("Loaded")

	def openConnexion(self):
		logging.warning("Try to connect")
		try:
			self.connexion = mysql.connector.connect(
				user='user',
				password='pass',
				host='maria',
				database='webapp'
			)
			self.cursor = self.connexion.cursor(prepared=True)
			logging.warning("Connected to database")
		except mysql.connector.Error as err:
			if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				logging.warning("Something is wrong with your user name or password")
			elif err.errno == errorcode.ER_BAD_DB_ERROR:
				logging.warning("Database does not exist")
			else:
				logging.warning(err)

	def closeConnexion(self):
		self.cursor.close()
		self.connexion.close()
		self.connexion = None
		self.cursor = None
		logging.info("Disconnected from database")


	def verifyLoginInformations(self, login, password):
		self.openConnexion()
		query = """
		SELECT COUNT(*) FROM Users
		WHERE login = %s AND password = %s
		"""
		input = (login, password)
		self.cursor.execute(query, input)
		results = self.cursor.fetchone()
		self.closeConnexion()
		return results[0]

	def addPost(self, text, publisherLogin):
		ret = ""
		self.openConnexion()
		try:
			query = """
			INSERT INTO Posts(date, text, publisher)
			VALUES(%s, %s, 
				(SELECT id FROM Users WHERE login = %s)
			)
			"""
			input = (datetime.now(), text, publisherLogin)
			self.cursor.execute(query, input)
			self.connexion.commit()
			ret = {"status": 0, "message": "Post successfully added"}
		except mysql.connector.Error as error :
			print("Failed to add post : {}".format(error))
			self.connexion.rollback()
			ret = {"status": 1, "message": "Failed to add post"}
		self.closeConnexion()
		return ret


	def getPosts(self):
		logging.warn("GOGOGO")
		self.openConnexion()
		postsList = []
		query = """
		SELECT Posts.id, Posts.date, Posts.text, Users.username
		FROM Posts
		INNER JOIN Users ON Users.id = Posts.publisher
		ORDER BY DATE DESC
		"""
		self.cursor.execute(query)
		results = self.cursor.fetchall()
		for row in results:
			post = {
				"id": row[0],
				"date": row[1].strftime("%m/%d/%Y"),
				"time": row[1].strftime("%H:%M:%S"),
				"text": row[2].decode("utf-8"),
				"publisher": row[3].decode("utf-8")
			}
			postsList.append(post)
		self.closeConnexion()
		return postsList
